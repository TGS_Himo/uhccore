package com.gmail.val59000mc.scenarios.scenariolisteners;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gmail.val59000mc.utils.VersionUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.players.UhcPlayer;
import com.gmail.val59000mc.scenarios.ScenarioListener;
import com.gmail.val59000mc.utils.RandomUtils;

public class RandomizedDropsListener extends ScenarioListener {

    private List<Material> items;
    private final Map<Material, ItemStack> dropList;

    public RandomizedDropsListener() {
        dropList = new HashMap<>();
    }

    @Override
    public void onEnable() {
        items = VersionUtils.getVersionUtils().getItemList();
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.isCancelled()) {
            return;
        }

        Block block = event.getBlock();

        ItemStack blockDrop;
        if (dropList.containsKey(block.getType())) {
            blockDrop = dropList.get(block.getType());
        } else {
            Material material = getRandomItem();
            blockDrop = new ItemStack(material);
            dropList.put(block.getType(), blockDrop);
        }

        event.setCancelled(true);
        block.setType(Material.AIR);
        block.getWorld().dropItem(block.getLocation().add(0.5, 0.5, 0.5), blockDrop);

        UhcPlayer.damageMiningTool(event.getPlayer(), 1);
    }

    private Material getRandomItem() {
        Material material;
        do {
            int itemIndex = RandomUtils.randomInteger(1, items.size()) - 1;
            material = items.get(itemIndex);
        } while (material == Material.WITHER_SKELETON_SPAWN_EGG || material == Material.SKELETON_SPAWN_EGG || material == Material.ZOMBIE_SPAWN_EGG || material == Material.WOLF_SPAWN_EGG || material == Material.POLAR_BEAR_SPAWN_EGG || material == Material.SILVERFISH_SPAWN_EGG || material == Material.TNT || material == Material.ENDERMAN_SPAWN_EGG); // Exclude both types of skeleton spawn eggs
        return material;
    }
}